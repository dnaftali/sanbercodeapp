import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
    },
    title: {
        color: '#036',
        fontSize: 26,
        fontWeight: 'bold',
    },
    image: {
        width: Dimensions.get('window').width * 0.5,
        height: Dimensions.get('window').width * 0.5,
        paddingVertical: 90
    },
    text: {
        color: '#rgba(0,0,0,.59)',
        fontWeight: 'bold',
        fontSize: 16,
        textAlign: "center",
    },
    buttonCircle: {
        width: 40,
        height: 40,
        backgroundColor: '#036',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textName: {
        fontSize: 24,
        color: '#003366',
        fontWeight: 'bold',
        padding: 4
    },
    form: {
		paddingHorizontal: 30,
		marginBottom: 10
	},
	textInput: {
		borderBottomWidth: 2,
		borderBottomColor: '#eee',
		borderStyle: 'solid',
		marginBottom: 20,
	},
	body: {
		marginTop: -25,
		marginHorizontal: 20,
		padding: 20,
		borderRadius: 10,
		backgroundColor: 'white',
		height: 200,
		elevation: 3,
	},
	input: {
		padding: 10,
		height: 36,
		borderColor: '#003366',
		borderWidth: 1
	},
	aksi: {
		marginTop: 30,
		height: 130,
		alignItems: 'center',
		justifyContent: 'space-between',
	},
    btnDark: {
		height: 30,
		backgroundColor: '#d23f31',
		width: 250,
		alignItems: 'center',
		justifyContent: 'center'
	},
    btnLight: {
		height: 30,
		backgroundColor: '#3EC6FF',
		width: 250,
		alignItems: 'center',
		justifyContent: 'center'
    },
    line: {
		marginVertical: 10,
		height: 1,
		borderWidth: 1,
		borderColor: '#ddd',
		borderStyle: 'solid',
    },
    btnContainer: {
		margin: 4,
		height: 40,
		justifyContent: 'center',
		borderRadius: 3
    },
    profilePicture: {
		width: 80,
		height: 80,
		borderRadius: 40,
    },
    header: {
		backgroundColor: '#3ec6ff',
		alignItems: 'center',
		height: 200,
		justifyContent: 'center',
	},
    lineContainer: {
        flexDirection: 'row'
    },
	btnTake: {
		alignSelf: 'center',
		backgroundColor: 'white',
		width: 70,
		height: 70,
		borderRadius: 35,
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 20
	},
	round: {
		alignSelf: 'center',
		width: 200,
		height: 280,
		borderRadius: 150,
		borderWidth: 2,
		borderColor: 'white',
		borderStyle: 'solid',
	},
	rectangle: {
		alignSelf: 'center',
		width: 200,
		height: 100,
		borderWidth: 2,
		borderColor: 'white',
		borderStyle: 'solid',
		marginTop: 40
    },
    logout: {
		margin: 4,
		height: 40,
		justifyContent: 'center',
        borderRadius: 3,
        backgroundColor: '#3ec6ff'
    }
})

export default styles