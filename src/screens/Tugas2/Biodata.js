import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, Dimensions } from 'react-native';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../../api/index';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { GoogleSignin } from '@react-native-community/google-signin';
import { CommonActions } from '@react-navigation/native';

export default function Biodata({ navigation }) {

    const [userInfo, setUserInfo] = useState(null)

    useEffect(() => {
        async function getToken () {
            try {
                const token = await AsyncStorage.getItem('token')
                if(token){
                    return getVenue(token)
                }
                console.log('getToken -> token', token)
            } catch(err) {
                console.log(err)
            }
        }
        getToken()
        getCurrentUser()
    }, [userInfo])

    const getCurrentUser = async () => {
        try{
            const userInfo = await GoogleSignin.signInSilently()
            console.log('getCurrentUser -> userInfo', userInfo)
            setUserInfo(userInfo)
        } catch (error){
            console.log(error)
        }
    }

    const getVenue = (token) => {
        Axios.get(`${api}/venues`, {
            timeout: 20000,
            headers: {
                'Authorization': 'Bearer' + token
            }
        })
        .then((res) => {
            console.log('profile -> res', res)
        })
        .catch((err) => {
            console.log('profile -> err', err)
        })
    }
    
    const onLogoutPress = async () => {
        try {
            if(userInfo) {
                await GoogleSignin.revokeAccess()
                await GoogleSignin.signOut()    
            }
            await AsyncStorage.removeItem('token')
            // navigation.dispatch(
            //     CommonActions.reset({
            //         index: 0,
            //         routes: [{ name: 'AuthStack'}]
            //     })
            // )
            navigation.navigate('Login')
        } catch(err) {
            console.log(err)
        }
    }

  return (
    <View style={styles.container}>
        <View style={{width: Dimensions.get('window').width, height: 220, backgroundColor: "#3EC6FF"}} >
            <View style={styles.body}>
                <View style={{ alignItems: "center"}}>
                    <Image source={{ uri: userInfo && userInfo.user && userInfo.user.photo}} style={styles.photo} />
                </View>
                <View style={{ alignItems: "center"}}>
                    <Text style={styles.textProfile}>
                        { userInfo && userInfo.user && userInfo.user.name}
                    </Text>
                </View>
                <View style={{alignItems: "center"}}>
                    <View style={styles.boxDetail}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <View style={styles.contentProfile}>
                                <Text>Tanggal lahir </Text>
                                <Text>Jenis Kelamin </Text>
                                <Text>Hobi </Text>
                                <Text>No. Telp </Text>
                                <Text>Email </Text>
                            </View>
                            <View style={styles.contentProfile}>
                                <Text style={{textAlign: 'right'}}>29 Mei 1976 </Text>
                                <Text style={{textAlign: 'right'}}>Laki - laki </Text>
                                <Text style={{textAlign: 'right'}}>Bobok Siang </Text>
                                <Text style={{textAlign: 'right'}}>08170214602 </Text>
                                <Text style={{textAlign: 'right'}}>{ userInfo && userInfo.user && userInfo.user.email} </Text>
                            </View>
                        </View>
                        <View style={{paddingTop: 20, alignItems: 'center'}}>
                            <TouchableOpacity style={{backgroundColor: '#3ec6ff', width: 200}} onPress={() => onLogoutPress()}>
                                <Text style={{ color: '#ffffff', textAlign: 'center' }}>LOGOUT</Text>
                            </TouchableOpacity>
                        </View>
                    </View> 
                </View>
            </View>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#fff',
  },
  photo: {
      height: 80,
      width: 80,
      borderRadius: 40
  },
  boxDetail: {
      padding: 20,
      width: Dimensions.get('window').width * 0.9,
      fontWeight: 'bold',
      backgroundColor: 'white',
      borderRadius:15,
      justifyContent: 'space-around',
      flexDirection: 'column',
      elevation: 3
    },    
    textProfile: {
        fontSize: 18,
        color: 'white',
        fontWeight: 'bold',
    },
    contentProfile: {
        flexDirection: 'column', 
        justifyContent: "space-between", 
        height: 150
    },
    body: {
        flexDirection: 'column', 
        justifyContent: 'space-between', 
        height: Dimensions.get('window').height * 0.6, 
        padding: 30 
    }
});
