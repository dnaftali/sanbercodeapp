import React, { useState } from 'react';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';  
import Ionicons from 'react-native-vector-icons/Ionicons';  
import Home from './Home';
import Profile from './Biodata';
import Maps from './Maps';
import Login from './Login';
import Chat from './Chat';
import SplashScreen from './Splashscreen';
import Intro from './Intro';
import ReactNative from '../../components/ReactNative';
import AsyncStorage from '@react-native-community/async-storage';

const Tabs = createBottomTabNavigator();
const Stack = createStackNavigator();

const HomeStack = () => {
  const [ isLoading, setIsLoading ] = useState(true)
  const [intro, setIntro] = useState(null)
  const [ isLogin, setIsLogin ] = useState('false')

  //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
  React.useEffect(() => {
      setTimeout(() => {
          setIsLoading(!isLoading)
      }, 3000)  
      async function getStatus() {
        const skipped = await AsyncStorage.getItem('skipped')
        setIntro(skipped)
        const skipLogin = await AsyncStorage.getItem('skipLogin')
        setIsLogin(skipLogin)

      }
      getStatus()
  }, [])



  if(isLoading) {
      return <SplashScreen />
  }

  return (
  <NavigationContainer>
    <Stack.Navigator initialRouteName={isLogin=='true' && 'Dashboard'}>
      { intro == null && <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} /> }
      <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
      <Stack.Screen name="Dashboard" component={Navigation} options={{ headerShown: false }} />
      <Stack.Screen name="ReactNative" component={ReactNative} options={{ headerShown: true }} />
    </Stack.Navigator>
  </NavigationContainer>
  )
}

const Navigation = ({ navigation }) => {
    return (
        <Tabs.Navigator initialRouteName="Home"
              screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, size, color }) => {
                  let iconName;
                  switch(route.name) {
                    case "Home": 
                      iconName = focused
                        ? 'home'
                        : 'home-outline';
                        return <MaterialCommunityIcons name={iconName} size={size} color={color} />;
                      break;
                      case "Maps": 
                      iconName = focused
                        ? 'map-marker-radius'
                        : 'map-marker-radius-outline';
                        return <MaterialCommunityIcons name={iconName} size={size} color={color} />;
                      break;
                      case "Chat": 
                      iconName = focused
                        ? 'md-chatbubbles'
                        : 'md-chatbubbles-outline';
                        return <Ionicons name={iconName} size={size} color={color} />;
                      break;
                      case "Profile": 
                      iconName = focused
                        ? 'person'
                        : 'person-outline';
                        return <Ionicons name={iconName} size={size} color={color} />;
                      break;
                  }
                },
              })}
              tabBarOptions={{
                activeTintColor: '#3EC6FF',
                inactiveTintColor: 'gray',
              }}
        >
          <Tabs.Screen name="Home" component={Home}/>
          <Tabs.Screen name="Maps" component={Maps} />
          <Tabs.Screen name="Chat" component={Chat} />
          <Tabs.Screen name="Profile" component={Profile} />
        </Tabs.Navigator>
      // <Container>
      //   <Header />
      //   <Content />
      //   <Footer>
      //     <FooterTab>
      //       <TouchableOpacity onPress={() => navigation.navigate('Home')}>
      //         <Button vertical>
      //           <Icon active name="home" />
      //           <Text>Home</Text>
      //         </Button>
      //       </TouchableOpacity>
      //       <Button vertical>
      //         <Icon name="body-outline" />
      //         <Text>Profile</Text>
      //       </Button>
      //     </FooterTab>
      //   </Footer>
      // </Container>
    );
  }

  export default HomeStack;
