import React from 'react';
import { View, Text, StyleSheet, StatusBar, Image, ScrollView, TouchableOpacity } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Home = ({ navigation }) => {
	return (
		<ScrollView>
		<View style={styles.container}>
            <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
			
			<View style={styles.card}>
				<View style={styles.cardHeader}>
					<Text style={styles.cardTitle}>Kelas</Text>
				</View>
				<View style={styles.cardFooter}>
					<View style={styles.row}>
						<TouchableOpacity onPress={() => navigation.navigate('ReactNative')}>
							<View style={styles.divIcon}>
								<Icon name="logo-react" size={wp('17%')} color={'#fff'} />
								<Text style={styles.normalText}>React Native</Text>
							</View>
						</TouchableOpacity>
						<View style={styles.divIcon}>
							<Icon name="logo-python" size={wp('17%')} color={'#fff'} />
							<Text style={styles.normalText}>React Native</Text>
						</View>
						<View style={styles.divIcon}>
							<Icon name="logo-react" size={wp('17%')} color={'#fff'} />
							<Text style={styles.normalText}>React JS</Text>
						</View>
						<View style={styles.divIcon}>
							<Icon name="logo-laravel" size={wp('17%')} color={'#fff'} />
							<Text style={styles.normalText}>Laravel</Text>
						</View>
					</View>
				</View>
			</View>


			<View style={styles.card}>
				<View style={styles.cardHeader}>
					<Text style={styles.cardTitle}>Kelas</Text>
				</View>
				<View style={styles.cardFooter}>
					<View style={styles.row}>
						<View style={styles.divIcon}>
							<Icon name="logo-wordpress" size={wp('17%')} color={'#fff'} />
							<Text style={styles.normalText}>Wordpress</Text>
						</View>
						<View style={styles.divIcon}>
							<Image style={styles.imageIcon} source={require('../../assets/icons/website-design.png')} />
							<Text style={styles.normalText}>Design Grafis</Text>
						</View>
						<View style={styles.divIcon}>
							<MaterialCommunityIcons name="server" size={wp('18.5%')} color={'#fff'} />
							<Text style={styles.normalText}>Web Server</Text>
						</View>
						<View style={styles.divIcon}>
							<Image style={styles.imageIcon} source={require('../../assets/icons/ux.png')} />
							<Text style={styles.normalText}>UI/UX Design</Text>
						</View>
					</View>
				</View>
			</View>


			<View style={styles.card}>
				<View style={styles.cardHeader}>
					<Text style={styles.cardTitle}>Summary</Text>
				</View>
				<View style={styles.cardListLight}>
					<Text style={styles.boldText}>React Native</Text>
				</View>
				<View style={styles.cardListDark}>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Today</Text>
						<Text style={styles.colTextRight}>20 orang</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Total</Text>
						<Text style={styles.colTextRight}>100 orang</Text>
					</View>
				</View>

				<View style={styles.cardListLight}>
					<Text style={styles.boldText}>Data Science</Text>
				</View>
				<View style={styles.cardListDark}>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Today</Text>
						<Text style={styles.colTextRight}>30 orang</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Total</Text>
						<Text style={styles.colTextRight}>100 orang</Text>
					</View>
				</View>

				<View style={styles.cardListLight}>
					<Text style={styles.boldText}>React JS</Text>
				</View>
				<View style={styles.cardListDark}>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Today</Text>
						<Text style={styles.colTextRight}>66 orang</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Total</Text>
						<Text style={styles.colTextRight}>100 orang</Text>
					</View>
				</View>

				<View style={styles.cardListLight}>
					<Text style={styles.boldText}>Laravel</Text>
				</View>
				<View style={styles.cardListDark}>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Today</Text>
						<Text style={styles.colTextRight}>66 orang</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Total</Text>
						<Text style={styles.colTextRight}>100 orang</Text>
					</View>
				</View>

				<View style={styles.cardListLight}>
					<Text style={styles.boldText}>WordPress</Text>
				</View>
				<View style={styles.cardListDark}>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Today</Text>
						<Text style={styles.colTextRight}>66 orang</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Total</Text>
						<Text style={styles.colTextRight}>100 orang</Text>
					</View>
				</View>

				<View style={styles.cardListLight}>
					<Text style={styles.boldText}>Design Grafis</Text>
				</View>
				<View style={styles.cardListDark}>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Today</Text>
						<Text style={styles.colTextRight}>66 orang</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Total</Text>
						<Text style={styles.colTextRight}>100 orang</Text>
					</View>
				</View>

				<View style={styles.cardListLight}>
					<Text style={styles.boldText}>Web Server</Text>
				</View>
				<View style={styles.cardListDark}>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Today</Text>
						<Text style={styles.colTextRight}>66 orang</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Total</Text>
						<Text style={styles.colTextRight}>100 orang</Text>
					</View>
				</View>

				<View style={styles.cardListLight}>
					<Text style={styles.boldText}>UI/UX Design</Text>
				</View>
				<View style={styles.cardListDark}>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Today</Text>
						<Text style={styles.colTextRight}>66 orang</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Total</Text>
						<Text style={styles.colTextRight}>100 orang</Text>
					</View>
				</View>

				<View style={styles.cardFooter}>
				</View>
			</View>
		</View>
		</ScrollView>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingHorizontal: wp('3%'),
		paddingTop: hp('2%'),
		backgroundColor: 'white',
	},
	card: {
		marginBottom: hp('2%'),
	},
	cardHeader: {
		backgroundColor: '#088dc4',
		borderTopLeftRadius: wp('2%'),
		borderTopRightRadius: wp('2%'),
		paddingVertical: wp('1%'),
		paddingHorizontal: wp('2%'),
	},
	cardListLight: {
		padding: wp('2%'),
		backgroundColor: '#3EC6FF',
	},
	cardListDark: {
		padding: wp('2%'),
		backgroundColor: '#088dc4',
	},
	cardFooter: {
		backgroundColor: '#3EC6FF',
		paddingVertical: wp('1%'),
		paddingHorizontal: wp('2%'),
		borderBottomLeftRadius: wp('2%'),
		borderBottomRightRadius: wp('2%'),
	},
	cardTitle: {
		color: '#ffffff',
		fontSize: wp('5%') 
	},
	divIcon: {
		alignItems: 'center'
	},
	imageIcon: {
		width: wp('18%'),
		height: hp('9.5%'),
	},
	normalText: {
		color: '#ffffff',
		fontSize: wp('4%'),
	},
	colText: {
		color: '#ffffff',
		fontSize: wp('4%'),
		width: wp('20%'),
	},
	colTextRight: {
		color: '#ffffff',
		fontSize: wp('4%'),
		width: wp('20%'),
		textAlign: 'right',
	},
	boldText: {
		color: '#ffffff',
		fontSize: wp('4%'),
		fontWeight: 'bold',
	},
	row: {
		flexDirection: 'row',
		justifyContent: 'space-around',
	},
	rowList: {
		flexDirection: 'row',
		justifyContent: 'space-around',
	}
});

export default Home;
