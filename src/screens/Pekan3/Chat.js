import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import styles from '../../style/styles';
import { GiftedChat } from 'react-native-gifted-chat';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';

const Chat = ({ route, navigation }) => {

    const [messages, setMessages] = useState([])
    const [user, setUser] = useState({})

    useEffect(() => {
        const user = auth().currentUser;
        setUser(user)
        getData()
        return () => {
            const db = database().ref('messages')
            if(db) {
                db.off()
            }
        }
    }, [])

    const getData = () => {
        database().ref('messages').limitToLast(20).on('child_added', snapshot => {
            const value = snapshot.val()
            setMessages(previousMessages => GiftedChat.append(previousMessages, value))
        })
    }

    const onSend = ((messages = []) => {
        for(let i=0; i < messages.length; i++) {
            database().ref('messages').push({
                _id: messages[i]._id,
                createdAt: database.ServerValue.TIMESTAMP,
                text: messages[i].text,
                user: messages[i].user
            })
        }
    })
    return (
        <GiftedChat
            messages={messages}
            onSend={messages => onSend(messages)}
            user={{
                _id: user.uid,
                name: user.email,
                avatar: 'https://lh3.googleusercontent.com/ogw/ADGmqu_6oebkuVwAOUBGEB_AOxzEMG-FpuQ9zhY3H6L6=s64-c-mo'
            }}
        />
    )
}

export default Chat