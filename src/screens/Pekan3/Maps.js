import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import styles from '../../style/styles';
import MapboxGL from '@react-native-mapbox-gl/maps';

MapboxGL.setAccessToken('pk.eyJ1IjoiZG5hZnRhbGkiLCJhIjoiY2tkZmVuY3dyMXEwZTJ4bDBjczRqOGVtcCJ9.NgHLyEw54ri6FOGIkV3DgA');

const Maps = () => {

    const [coordinates, setCoordinates] = useState([
        [107.598827, -6.896191],
        [107.596198, -6.899688],
        [107.618767, -6.902226],
        [107.621095, -6.898690],
        [107.615698, -6.896741],
        [107.613544, -6.897713],
        [107.613697, -6.893795],
        [107.610714, -6.891356],
        [107.605468, -6.893124],
        [107.609180, -6.898013]
    ])

    useEffect(() => {
        const getLocation = async() => {
            try {
                const permission = await MapboxGL.requestAndroidLocationPermissions()
            } catch(error) {
                console.log(error)
            }
        }
        getLocation();
    }, [])

    const CoordinatesAnnotation = () => {
        return coordinates.map((data) => {
            let [longitude, latitude] = data;
            console.log((longitude+latitude).toString());
            return(
                <MapboxGL.PointAnnotation
                    key={longitude+latitude}
                    id={(longitude+latitude).toString()}
                    coordinate={data}
                >
                    <MapboxGL.Callout
                        key={longitude+latitude}
                        title={"Longitude: "+ longitude + " Latitude: " + latitude}
                    />
                </MapboxGL.PointAnnotation>
            )
        })
    }

    return (
        <View style={styles.container}>
            <MapboxGL.MapView
                style={{ flex: 1 }}
            >
                <MapboxGL.UserLocation
                    visible={true}
                />
                <MapboxGL.Camera
                    followUserLocation={true}
                />
                <CoordinatesAnnotation />
                {/* <MapboxGL.PointAnnotation
                    id='pointAnnotation'
                    coordinate={[107.61074, -6.891356]}
                >
                    <MapboxGL.Callout
                        title="Ini adalah marker"
                    />
                </MapboxGL.PointAnnotation> */}
            </MapboxGL.MapView>
        </View>
    )
}

export default Maps