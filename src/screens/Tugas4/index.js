import React, {useState, createContext} from 'react';
import TodoList from './TodoList';

export const RootContext = createContext();

const Context = () => {
    const [textInput, setTextInput] = useState('');
    const [list, setList] = useState([]);
    const [extraData, setExtraData] = useState(true);

    handleChangeInput = (text) => {
        setTextInput(text)
    }

    addList = () => {
        let d = new Date();
        let strDate = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
        
        setList([...list, {date: strDate, text: textInput}])
        setTextInput('');
    }

    delList = (index) => {
        list.splice(index, 1);
        setExtraData(!extraData)
    }

    return (
        <RootContext.Provider value={{
            list,
            textInput,
            handleChangeInput,
            addList,
            delList
        }}>
            <TodoList />
        </RootContext.Provider>
    )
}
export default Context;