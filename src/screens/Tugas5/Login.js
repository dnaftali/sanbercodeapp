import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    Button,
    TextInput,
    Alert,
    StatusBar,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import styles from '../../style/styles';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../../api/index';
import auth from '@react-native-firebase/auth';
import { CommonActions } from '@react-navigation/native';
import { GoogleSignin, StatusCodes, GoogleSigninButton } from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id';

const config = {
    title: 'Authentication Required',
    imageColor: '#191970',
    imageErrorColor: 'red',
    sensorDescription: 'Touch Sensor',
    sensorErrorDescription: 'Failed',
    cancelText: 'Cancel'
}

const Login = ({ navigation }) => {
    const [email, setEmail] =  useState('');
    const [password, setPassword] = useState('');
    const [isLoading, setIsLoading] = useState(false)

    const saveToken = async (token) => {
        try {
            await AsyncStorage.setItem('token', token)
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        configureGoogleSignIn()
    }, [])

    const configureGoogleSignIn = () => {
        GoogleSignin.configure({
            offlineAccess: false,
            webClientId: '834698837668-35gv1fr9damqghbv6r63emqe57p2v7du.apps.googleusercontent.com'
        })
    }

    const signInWithGoogle = async () => {
        try {
            const { idToken } = await GoogleSignin.signIn()
            console.log('signInWithGoogle -> idToken', idToken)

            const credential = auth.GoogleAuthProvider.credential(idToken)

            auth().signInWithCredential(credential)

            navigation.navigate('Profile')
        } catch (error) {
            console.log('signInWithGoogle -> error', error);
            Alert.alert('Google signin error. Please try again')
        }
    }

    const onLoginProcess = () => {
        return auth().signInWithEmailAndPassword(email, password)
        .then((res) => {
            navigation.navigate('Home')
        })
        .catch((err) => {
            console.log(err)
        })
    }

    // const onLoginProcess = () => {
    //     let data = {
    //         email: email,
    //         password: password
    //     }
    //     Axios.post(`${api}/login`, data, {
    //         timeout: 20000
    //     })
    //     .then((res) => {
    //         console.log('login -> res', res)
    //         saveToken(res.data.token)
    //         navigation.navigate('Profile')
    //     })
    //     .catch((err) => {
    //         console.log('login -> err', err)
    //         Alert.alert('Invalid username or password. Please try again')
    //     })
    // }

    const signInWIthFingerPrint = () => {
        TouchID.authenticate('', config)
        .then(success => {
            alert('Authentication Success')
        })
        .catch(error => {
            alert('Authentication Failed')
        })
    }

    if(isLoading) {
        return <ActivityIndicator size='large' color='#3EC6FF' />
    }

    return (
        <View style={styles.container}>
                <View style={{paddingTop: 20}}>
                    <View style={{marginTop: 59, alignItems: 'center'}}>
                        <Image source={require('../../assets/images/logo.jpg')} style={{height: 100, width: 250}} />
                    </View>
                    <View style={{alignItems:'center', padding: 20}}>
                        <Text style={[styles.textName, styles.label]}>
                            Login
                        </Text>
                    </View>
                    <View style={{paddingTop: 20}}>
                        <View style={styles.form}>
                            <Text style={styles.label}>Username / Email</Text>
                            <TextInput style={styles.input} 
                                value={email}
                                underlineColorAndroid='#c6c6c6'
                                placeholder='Username or Email'
                                onChangeText={(email) => setEmail(email)}
                            />
                        </View>
                        <View style={styles.form}>
                            <Text style={styles.label}>Password</Text>
                            <TextInput style={styles.input} 
                                secureTextEntry
                                value={password}
                                underlineColorAndroid='#c6c6c6'
                                placeholder='Password'
                                onChangeText={(password) => setPassword(password)}
                            />
                        </View>
                        <View style={styles.aksi}>
                            <View>
                                <Button 
                                    color="#3EC6FF"
                                    title="LOGIN"
                                    onPress={() => onLoginProcess()}
                                />
                            </View>
                            {/* <View style={styles.line}></View> */}
                                <Text style={{ marginTop: 10 }}>OR</Text>
                                {/* <View style={styles.line}></View> */}
                            <View style={styles.lineContainer}>
                                <GoogleSigninButton
                                    onPress={() => signInWithGoogle()}
                                    style={{ width: '100%', height: 40 }}
                                    size={GoogleSigninButton.Size.Wide}
                                    color={GoogleSigninButton.Color.Dark}
                                />
                            </View>
                            <View>
                                    <Button 
                                        color="#191970"
                                        title="Sign in with Fingerprint"
                                        onPress={() => signInWIthFingerPrint()}
                                    />
                            </View>
                            <View style={styles.loginFooter}>
                    <Text style={styles.formLabel}>Belum mempunyai akun ?</Text>
                    <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                        <Text style={{color:'#3598db'}}> Buat Akun</Text>
                    </TouchableOpacity>
                </View>

                        </View>
				    </View>                
                </View>
            </View>
    )
}

export default Login;