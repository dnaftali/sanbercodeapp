import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    Modal,
    TextInput,
    TouchableOpacity
} from 'react-native';
import styles from '../style/styles';
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/Feather';
import storage from '@react-native-firebase/storage';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import auth from '@react-native-firebase/auth';


export default function Register ({ navigation }) {
    let camera
	const [isVisible, setIsVisible ] = useState(false)
	const [type, setType ] = useState('back')
	const [photo, setPhoto ] = useState(null)

    useEffect(() => {
		auth().signInAnonymously()
	}, [])

	const toggleCamera = () => {
		setType(type === 'back' ? 'front' : 'back')
	}

	const takePicture = async () => {
		const options = { quality: 0.5, base64: true }
		if(camera) {
			const data = await camera.takePictureAsync(options)
			setPhoto(data)
			setIsVisible(false)
		}
	}

	const uploadImage = (uri) => {
        // alert('Tombol dipencet')
		const sessionId = new Date().getTime()
		return storage()
		.ref(`images/${sessionId}`)
		.putFile(uri)
		.then((response) => {
			alert('Upload Success')
		})
		.catch((error) => {
			alert(error)
		})
	}

    const renderCamera = () => {
		return (
			<Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
				<View style={{flex: 1}}>
					<RNCamera
						style={{flex: 1, justifyContent: 'space-around', flexDirection: 'column'}}
						ref={ref => {camera = ref;}}
						type={type}
					>
						<View style={styles.btnContainer}>
							<TouchableOpacity style={styles.btnFlip} onPress={() => toggleCamera()}>
								<MaterialCommunity name="rotate-3d-variant" size={15} />
							</TouchableOpacity>
						</View>
						<View style={styles.round} />
						<View style={styles.rectangle} />
						<View style={styles.btnContainer}>
							<TouchableOpacity style={styles.btnTake} onPress={() => takePicture()}>
								<Icon name="camera" size={30} />
							</TouchableOpacity>
						</View>
					</RNCamera>
				</View>
			</Modal>
		)
    }
    
    return (
		<View style={styles.container}>
			{renderCamera()}
			<View style={styles.header}>
				<Image
					style={styles.profilePicture}
					source={photo === null ? require('../assets/images/photo.jpg') : { uri: photo.uri }}
				/>
				<TouchableOpacity onPress={() => setIsVisible(true)}>
					<Text style={styles.btnChangePicture}>Change Picture</Text>
				</TouchableOpacity>
			</View>
			<View style={[styles.body,{height: 350}]}>
				<View style={styles.col}>
					<Text style={[styles.text, {textAlign: 'left'}]}>Nama</Text>
                    <TextInput
                        placeholder='Nama'
                        style={styles.textInput}
                    />
				</View>
				<View style={styles.col}>
					<Text style={[styles.text, {textAlign: 'left'}]}>Email</Text>
                    <TextInput
                        placeholder='Email'
                        style={styles.textInput}
                    />
				</View>
				<View style={styles.col}>
					<Text style={[styles.text, {textAlign: 'left'}]}>Password</Text>
                    <TextInput
                        placeholder='Password'
                        style={styles.textInput}
                    />
				</View>
				<View style={styles.btnContainer}>
                    <TouchableOpacity style={styles.logout} onPress={() => uploadImage(photo.uri)}>
                            <Text style={{ textAlign: 'center', color: 'white' }}>REGISTER</Text>
                    </TouchableOpacity>
				</View>
			</View>
		</View>
	)
}