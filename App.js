import React from 'react';
// import Intro from './src/screens/Tugas1/Intro';
// import Biodata from './src/screens/Tugas2/Biodata';
// import TodoList from './src/screens/Tugas3/TodoList';
// import TodoList from './src/screens/Tugas4/index';
//import Navigation from './src/screens/Tugas5/navigation';
import Navigation from './src/screens/Pekan3/Navigation';
import Home from './src/screens/Pekan3/Home';
import Maps from './src/screens/Pekan3/Maps';
import ReactNative from './src/components/ReactNative';
import Login from './src/screens/Tugas5/Login';

import firebase from '@react-native-firebase/app';

var firebaseConfig = {
  apiKey: "AIzaSyAVNssHnbpUpKft54ioujPRyct1ROPNFng",
  authDomain: "sanbercode-78d05.firebaseapp.com",
  databaseURL: "https://sanbercode-78d05.firebaseio.com",
  projectId: "sanbercode-78d05",
  storageBucket: "sanbercode-78d05.appspot.com",
  messagingSenderId: "834698837668",
  appId: "1:834698837668:web:d801c36969dfaaf3bcb031",
  measurementId: "G-X3K77RVXYD"
};

// Inisialisasi firebase
if(!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

export default function App() {
  return (
    <Navigation />
  );
}
